#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System;

public static class AlignTransformTool
{
    [MenuItem("Custom/Distribute Children")]
    private static void DistributeTransforms()
    {
        if ( Selection.activeTransform == null || Selection.activeTransform.childCount < 2) return;
        Vector3 min = Selection.activeTransform.GetChild(0).position;
        Vector3 max = Selection.activeTransform.GetChild(Selection.activeTransform.childCount-1).position;
        for(int i = 0; i < Selection.activeTransform.childCount; i++)
        {
            Selection.activeTransform.GetChild(i).position = Vector3.Lerp(min, max, i * 1f / (Selection.activeTransform.childCount-1f));
        }
    }

   
}

#endif