//HierarchyHighlighter.cs
using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
 
[InitializeOnLoad]
public class HierarchyHighlighter {
 
    static HierarchyHighlighter() {
        EditorApplication.hierarchyWindowItemOnGUI -= HierarchyHighlight_OnGUI;
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyHighlight_OnGUI;
    }
    
    public static readonly Color DEFAULT_COLOR_HIERARCHY_SELECTED = new Color(0.243f, 0.4901f, 0.9058f, 1f);

    private static void HierarchyHighlight_OnGUI(int inSelectionID, Rect inSelectionRect)
    {
        GameObject GO_Label = EditorUtility.InstanceIDToObject(inSelectionID) as GameObject;
        if (GO_Label != null)
        {
            var Label = GO_Label.GetComponent<HierarchyHighlighterComponent>();

            if(Label != null && Event.current.type == EventType.Repaint)
            {
                #region Determine Styling

                bool ObjectIsSelected = Selection.instanceIDs.Contains(inSelectionID);

                Color BKCol = Label.color;
                Color TextCol = Label.textColor;
                FontStyle TextStyle = Label.TextStyle;

                if(!Label.isActiveAndEnabled)
                {
                    if(BKCol != HierarchyHighlighterComponent.DEFAULT_BACKGROUND_COLOR)
                        BKCol.a = BKCol.a * 0.5f; //Reduce opacity by half

                    TextCol.a = TextCol.a * 0.5f;
                }

                #endregion

                Rect Offset = new Rect(inSelectionRect.position + new Vector2(2f, 0f), inSelectionRect.size);

                #region Draw Background

                //Only draw background if background color is not completely transparent
                if (BKCol.a > 0f)
                {
                    Rect BackgroundOffset = new Rect(inSelectionRect.position, inSelectionRect.size);

                    //If the background has transparency, draw a solid color first
                    if (Label.color.a < 1f || ObjectIsSelected)
                    {
                        //ToDo: Pull background color from GUI.skin Style
                        EditorGUI.DrawRect(BackgroundOffset, HierarchyHighlighterComponent.DEFAULT_BACKGROUND_COLOR);
                    }

                    //Draw background
                    if (ObjectIsSelected)
                        EditorGUI.DrawRect(BackgroundOffset, Color.Lerp(GUI.skin.settings.selectionColor, BKCol, 0.3f));
                    else
                        EditorGUI.DrawRect(BackgroundOffset, BKCol);
                }

                #endregion


                EditorGUI.LabelField(Offset, GO_Label.name, new GUIStyle()
                {
                    normal = new GUIStyleState() { textColor = TextCol },
                    fontStyle = TextStyle
                });

                EditorApplication.RepaintHierarchyWindow();
            }
        }
    }
}
#endif
 
public class HierarchyHighlighterComponent : MonoBehaviour {
    
    public static readonly Color DEFAULT_BACKGROUND_COLOR = new Color(0.76f, 0.76f, 0.76f, 1f);
    public static readonly Color DEFAULT_BACKGROUND_COLOR_INACTIVE = new Color(0.306f, 0.396f, 0.612f, 1f);
    public static readonly Color DEFAULT_TEXT_COLOR = Color.black;

    public Color color = DEFAULT_BACKGROUND_COLOR;
    public Color textColor = DEFAULT_TEXT_COLOR;
    public FontStyle TextStyle = FontStyle.Normal;
}
