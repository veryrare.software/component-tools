 #if UNITY_EDITOR
 using UnityEditor;
 using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 using System.Text.RegularExpressions;
 
public class BetterDuplicate
{
    [InitializeOnLoadMethod]
    static void Init()
    {
        EditorSettings.gameObjectNamingScheme = EditorSettings.NamingScheme.Underscore;
    }

    [MenuItem("Custom/Duplicate Adjacently #%d")]
    static void Duplicate()
    {
        GameObject selectedObject = Selection.activeGameObject;
        if ( selectedObject == null) return;

        var prefabInstanceRoot = PrefabUtility.GetOutermostPrefabInstanceRoot(selectedObject);
        GameObject prefabParent = prefabInstanceRoot == selectedObject ? PrefabUtility.GetCorrespondingObjectFromSource(selectedObject) : null;

        // Create new object
        GameObject duplicatedObject =
            prefabParent != null ? PrefabUtility.InstantiatePrefab(prefabParent, selectedObject.transform.parent) as GameObject : GameObject.Instantiate(selectedObject, selectedObject.transform.parent);
        
        var newName = selectedObject.name;
        var idx = 0;
        if ( EditorSettings.NamingScheme.SpaceParenthesis == EditorSettings.gameObjectNamingScheme ) 
        {
            var separatorIdx = newName.LastIndexOf(' ');
            if ( separatorIdx >= 0 && separatorIdx < newName.Length)
            {
                var numStr = newName.Substring(separatorIdx);
                numStr = numStr.Replace("(","");
                numStr = numStr.Replace(")","");
                if ( int.TryParse(numStr, out idx))
                {
                    newName = newName.Substring(0,separatorIdx) + " ("+(idx+1).ToString("D"+EditorSettings.gameObjectNamingDigits) + ")";
                } else 
                {
                    newName += " ("+1.ToString("D"+EditorSettings.gameObjectNamingDigits) + ")";
                }
            } else 
            {
                newName += " ("+1.ToString("D"+EditorSettings.gameObjectNamingDigits) + ")";
            }
        } else if ( 
            EditorSettings.NamingScheme.Underscore == EditorSettings.gameObjectNamingScheme 
            ||  EditorSettings.NamingScheme.Dot == EditorSettings.gameObjectNamingScheme)
        {
            char c = EditorSettings.NamingScheme.Underscore == EditorSettings.gameObjectNamingScheme ? '_' : '.';
            var separatorIdx = newName.LastIndexOf(c);
            if ( separatorIdx >= 0 && separatorIdx < newName.Length)
            {
                var numStr = newName.Substring(separatorIdx+1);
                if ( int.TryParse(numStr, out idx))
                {
                    newName = newName.Substring(0,separatorIdx) + c+(idx+1).ToString("D"+EditorSettings.gameObjectNamingDigits);
                } else 
                {
                    newName += c+1.ToString("D"+EditorSettings.gameObjectNamingDigits);
                }
            } else 
            {
                newName += c+1.ToString("D"+EditorSettings.gameObjectNamingDigits);
            }
        }
        bool isPrefab = false;
        var parent = selectedObject.transform.parent;
        while( parent != null )
        {
            if ( PrefabUtility.IsPartOfAnyPrefab(parent)){
                isPrefab = true;
                break;
            }
            parent = parent.parent;
        }
        duplicatedObject.name = newName;
        Undo.RegisterCreatedObjectUndo(duplicatedObject, "Duplicate Adjacently");
        duplicatedObject.SetActive(selectedObject.activeSelf);
        // {
            duplicatedObject.transform.localPosition = selectedObject.transform.localPosition;
            duplicatedObject.transform.localRotation = selectedObject.transform.localRotation;
            duplicatedObject.transform.localScale = selectedObject.transform.localScale;
            if (! isPrefab )
                duplicatedObject.transform.SetSiblingIndex(selectedObject.transform.GetSiblingIndex() + 1);
        // } else {
        //     duplicatedObject.transform.position = selectedObject.transform.position;
        //     duplicatedObject.transform.rotation = selectedObject.transform.rotation;
        //     duplicatedObject.transform.localScale = selectedObject.transform.lossyScale;
        // }
        Selection.activeGameObject = duplicatedObject;
    }

}

#endif
