#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
 
public static class ComponentTools
{
    [MenuItem("Custom/Toggle Collapse/Expand Inspector #%m")]
    private static void ToggleAll(MenuCommand command)
    {
        var isExpanded = InternalEditorUtility.GetIsInspectorExpanded(Selection.activeGameObject.GetComponent<Component>());
        SetAllInspectorsExpanded(Selection.activeGameObject, !isExpanded);
    }

    [MenuItem("Custom/Collapse Inspector")]
    private static void CollapseAll(MenuCommand command)
    {
        SetAllInspectorsExpanded(Selection.activeGameObject, false);

    }
 
    [MenuItem("Custom/Expand Inspector")]
    private static void ExpandAll(MenuCommand command)
    {
        SetAllInspectorsExpanded(Selection.activeGameObject, true);
    }
 
    public static void SetAllInspectorsExpanded(GameObject go, bool expanded)
    {
        Component[] components = go.GetComponents<Component>();
        foreach (Component component in components)
        {
            if (component is Renderer)
            {
                var mats = ((Renderer)component).sharedMaterials;
                for (int i = 0; i < mats.Length; ++i)
                {
                    InternalEditorUtility.SetIsInspectorExpanded(mats[i], expanded);
                }
            }
            InternalEditorUtility.SetIsInspectorExpanded(component, expanded);
        }
        ActiveEditorTracker.sharedTracker.ForceRebuild();
    }
   
}

#endif