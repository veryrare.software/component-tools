#if UNITY_EDITOR
using System.Reflection;
using UnityEditor.Overlays;
using System;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

[Overlay(typeof(UnityEditor.SceneView), "CustomGridOverlay", "Grid Info")]
public class CustomGridOverlay : Overlay
{
    private VisualElement root;
    private Vector3Field vectorField;
    private Slider gridSizeX;
    private Slider gridSizeY;
    private Slider gridSizeZ;
    private Assembly assembly;
    private Type gridSettings;
    private PropertyInfo gridSize;

    public static Vector3 GetUnityGridSize_EDITOR_ONLY()
    {
        var assembly = Assembly.Load("UnityEditor.dll");
        var gridSettings = assembly.GetType("UnityEditor.GridSettings");
        var gridSize = gridSettings.GetProperty("size");
        var v = (Vector3) gridSize.GetValue("size");
        return v;
    }

    public override VisualElement CreatePanelContent()
    {
        root = new VisualElement();
        root.style.width = new StyleLength(150f);

        assembly = Assembly.Load("UnityEditor.dll");
        gridSettings = assembly.GetType("UnityEditor.GridSettings");
        gridSize = gridSettings.GetProperty("size");

        Vector3 v = (Vector3) gridSize.GetValue("size");
        vectorField = new Vector3Field();
        vectorField.RegisterValueChangedCallback(callbackV);

        gridSizeX = new Slider(0f, 20.0f, SliderDirection.Horizontal, 0.1f);
        gridSizeX.RegisterValueChangedCallback(callback);
        gridSizeY = new Slider(0f, 20.0f, SliderDirection.Horizontal, 0.1f);
        gridSizeY.RegisterValueChangedCallback(callback);
        gridSizeZ = new Slider(0f, 20.0f, SliderDirection.Horizontal, 0.1f);
        gridSizeZ.RegisterValueChangedCallback(callback);

        root.Add(vectorField);
        root.Add(gridSizeX);
        root.Add(gridSizeY);
        root.Add(gridSizeZ);

        root.schedule.Execute(SetValues).Every(100);
        SetValues();
        return root;
    }

    void SetValues()
    {
            Vector3 v = (Vector3) gridSize.GetValue("size");
            gridSizeX.value = v.x;
            gridSizeY.value = v.y;
            gridSizeZ.value = v.z;
            vectorField.value = v;
    }

    private void callbackV(ChangeEvent<Vector3> evt)
    {
        gridSize.SetValue("size", vectorField.value);
    }
    
    private void callback(ChangeEvent<float> evt)
    {
        gridSize.SetValue("size", new Vector3(gridSizeX.value,gridSizeY.value,gridSizeZ.value));
    }

}
#endif