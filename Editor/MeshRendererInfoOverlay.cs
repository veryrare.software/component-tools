#if UNITY_EDITOR
using UnityEditor.Overlays;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using System.Globalization;

[Overlay(typeof(UnityEditor.SceneView), "Mesh Info Overlay", "Mesh Info")]
public class MeshRendererInfoOverlay : Overlay
{
    private VisualElement root;
    private Label polyCountField;
    private Label subMeshesField;
    private Label materialsField;

    public override VisualElement CreatePanelContent()
    {
        root = new VisualElement();
        root.style.width = new StyleLength(150f);

        root.Add(polyCountField = new Label());
        root.Add(subMeshesField = new Label());
        root.Add(materialsField = new Label());

        root.schedule.Execute(SetValues).Every(100);
        SetValues();
        return root;
    }

    void SetValues()
    {
        int polyCount = 0;
        int subMeshes = 0;
        int materials = 0;
        foreach(GameObject g in Selection.gameObjects)
        {
            if ( g.TryGetComponent<MeshRenderer>(out var r))
            {
                if ( r.TryGetComponent<MeshFilter>(out var mf) && mf.sharedMesh != null)
                {
                    polyCount += mf.sharedMesh.triangles.Length;
                    subMeshes += mf.sharedMesh.subMeshCount;
                }
                materials += r.sharedMaterials.Length;
            }
            else if ( g.TryGetComponent<SkinnedMeshRenderer>(out var s) && s.sharedMesh != null)
            {
                polyCount += s.sharedMesh.triangles.Length;
                subMeshes += s.sharedMesh.subMeshCount;
                materials += s.sharedMaterials.Length;
            }
        }
        polyCount/=3;
        var f = new NumberFormatInfo {NumberGroupSeparator = " "};
        polyCountField.text = "Triangles: " + polyCount.ToString("#,#", f);
        subMeshesField.text = "Submeshes: " + subMeshes.ToString("#,#", f);
        materialsField.text = "Materials: " + materials.ToString("#,#", f);
    }

}
#endif