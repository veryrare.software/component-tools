using System.Collections.Generic;

public class MeanAvg<T>
{
    int maxCount = 10;
    Queue<T> queue = new Queue<T>();
    T avg = default(T);

    public MeanAvg(int maxCount = 10)
    {
        this.maxCount = maxCount;
    }

    public T Add(T entry)
    {
#if NET_4_6
        if ( queue.Count >= maxCount) queue.Dequeue();
        queue.Enqueue(entry);
        dynamic avg = default(T);
        foreach(T i in queue)
        {
            avg = avg + i;
        }
        avg /= queue.Count;
        this.avg = avg;
#endif
        return avg;
    }

    public T GetAvg()=>avg;
}