using System;
using UnityEngine;

public static class MathParabola
{
    public static Func<float, float, float> f = (x, height) => -4 * height * x * x + 4 * height * x;

    public static Vector3 Parabola(Vector3 start, Vector3 end, Vector3 up, float height, float t)
    {
        return Vector3.Lerp(start, end, t) + up * f(t, height);
    }

    public static Vector3 Parabola(Vector3 start, Vector3 end, float height, float t)
    {
        var mid = Vector3.Lerp(start, end, t);

        return new Vector3(mid.x, f(t, height) + mid.y, mid.z);
    }

    public static Vector2 Parabola(Vector2 start, Vector2 end, float height, float t)
    {
        var mid = Vector2.Lerp(start, end, t);

        return new Vector2(mid.x, f(t, height) + mid.y);
    }

}