using UnityEngine;

public static class CapsuleColliderWorldPositionExtension
{
    public static Vector3 Position(this CapsuleCollider c) 
    {
        return c.transform.TransformPoint(c.center); 
    }

    public static Vector3 BottomPosition(this CapsuleCollider c) 
    { 
        return c.transform.TransformPoint(c.center - new Vector3(0f, c.height * 0.5f, 0f)); 
    }

    public static Vector3 BottomSpherePosition(this CapsuleCollider c) 
    { 
        return c.transform.TransformPoint(c.center - new Vector3(0f, c.height * 0.5f - c.radius, 0f)); 
    }

    public static Vector3 Position(this CharacterController c) 
    {
        return c.transform.TransformPoint(c.center); 
    }

    public static Vector3 BottomPosition(this CharacterController c) 
    { 
        return c.transform.TransformPoint(c.center - new Vector3(0f, c.height * 0.5f, 0f)); 
    }

    public static Vector3 BottomSpherePosition(this CharacterController c) 
    { 
        return c.transform.TransformPoint(c.center - new Vector3(0f, c.height * 0.5f - c.radius, 0f)); 
    }

    public static Vector3 TopSpherePosition(this CharacterController c) 
    { 
        return c.transform.TransformPoint(c.center + new Vector3(0f, c.height * 0.5f - c.radius, 0f)); 
    }


}
