using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class  VectorUtility
{
    public static Vector2 xy(this Vector3 v) => new Vector2(v.x, v.y);
    public static Vector2 yx(this Vector3 v) => new Vector2(v.y, v.x);
    public static Vector2 xz(this Vector3 v) => new Vector2(v.x, v.z);
    public static Vector2 zx(this Vector3 v) => new Vector2(v.z, v.x);
    public static Vector2 zy(this Vector3 v) => new Vector2(v.z, v.y);
    public static Vector2 yz(this Vector3 v) => new Vector2(v.y, v.z);

    public static Vector3 xzy(this Vector3 v) => new Vector3(v.x, v.z, v.y);
    public static Vector3 zxy(this Vector3 v) => new Vector3(v.z, v.x, v.y);
    public static Vector3 zyx(this Vector3 v) => new Vector3(v.z, v.y, v.x);
    public static Vector3 yzx(this Vector3 v) => new Vector3(v.y, v.z, v.x);
    public static Vector3 yxz(this Vector3 v) => new Vector3(v.y, v.x, v.z);

    public static Vector3 XZ(this Vector3 a) 
    { 
        var tmp = a;
        tmp.y = 0f;
        return tmp;
    }

    public static Vector3 XZ_Norm(this Vector3 a) => a.XZ().normalized;

    public static float XZDistance(this Vector3 a, Vector3 b) 
    { 
        var tmp = a - b;
        tmp.y = 0f;
        return tmp.magnitude;
    }

    public static float Distance(this Vector3 a, Vector3 b) => Vector3.Distance(a,b);
    
    // DISTANCES FOR TRANSFORMS:
    public static float XZDistance(this Transform a, Vector3 b) => a.position.XZDistance(b);
    
    public static float Distance(this Transform a, Vector3 b) => a.position.Distance(b);
    
    public static float XZDistance(this Transform a, Transform b) => a.position.XZDistance(b.position);
    
    public static float Distance(this Transform a, Transform b) => Vector3.Distance(a.position,b.position);
    
}
