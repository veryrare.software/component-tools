using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public static class GameObjectAddComponentExtension 
{
    public static T GetOrAddComponent<T>(this GameObject o, T original) where T : Component
    {
        if ( o.TryGetComponent<T>(out T t)) return t;
        return o.AddComponent<T>(original);
    }

    public static T GetOrAddComponent<T>(this GameObject o) where T : Component
    {
        if ( o.TryGetComponent<T>(out T t)) return t;
        return o.AddComponent<T>();
    }

    public static T AddComponent<T>(this GameObject destination, T original) where T : Component
    {
        if (original == null) return destination.AddComponent<T>();

        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        // Copied fields can be restricted with BindingFlags
        System.Reflection.FieldInfo[] fields = type.GetFields(); 
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }

        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos) {
            if (pinfo.CanWrite) {
                try {
                    pinfo.SetValue(copy, pinfo.GetValue(original, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }

        foreach (PropertyInfo x in typeof ( T ).GetProperties ())
        if (x.CanWrite)
            x.SetValue ( copy, x.GetValue ( original ) );
        
        return copy as T;
    }

    public static Transform RecursiveFindChild(this Transform parent, string childName)
    {
        Queue<Transform> queue = new Queue<Transform>();
        queue.Enqueue(parent);
        while (queue.Count > 0)
        {
            var c = queue.Dequeue();
            if ( c.name == childName) return c;
            foreach(Transform t in c) queue.Enqueue(t);
        }
        return null;
    }
}