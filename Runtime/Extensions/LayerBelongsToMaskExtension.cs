using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LayerMaskExtension 
{
    public static bool Contains(this LayerMask layermask, int layer)
    {
        return layermask == (layermask | (1 << layer));
    }

    public static int ToLayerIndex ( this LayerMask bitmask ) 
    {
        int result = bitmask>0 ? 0 : 31;
        while( bitmask>1 ) {
            bitmask = bitmask>>1;
            result++;
        }
        return result;
    }

    public static LayerMask LayerIndexToMask(int layerIndex)
    {
        return 1 << layerIndex;
    }

}
