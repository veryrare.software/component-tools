using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;

public static class ForceTPose
{
    
    public static List<Transform> Traverse(this Transform parent)
    {
        List<Transform> children = new List<Transform>();
        TraverseRecursive(parent, children);
        return children;
    }

    private static void TraverseRecursive(Transform parent, List<Transform> t)
    {
        t.Add(parent);
        foreach(Transform child in parent)
            TraverseRecursive(child, t);
    }


    [MenuItem("Custom/Force T-Pose")]
    static void TPose()
    {
        var selected = Selection.activeGameObject;
        if (!selected)
            return;
 
        if (!selected.TryGetComponent<Animator>(out var animator))
            return;
 
        var skeletons=animator.avatar?.humanDescription.skeleton;
     
        var root = animator.transform;
        var tfs = root.Traverse();
        var dir = new Dictionary<string, Transform>(tfs.Count());
        foreach (var tf in tfs)
        {
            if (!dir.ContainsKey(tf.name)) dir.Add(tf.name, tf);
        }
 
        foreach (var skeleton in skeletons)
        {
            if (!dir.TryGetValue(skeleton.name, out var bone)) continue;
            bone.localPosition = skeleton.position;
            bone.localRotation = skeleton.rotation;
            bone.localScale = skeleton.scale;
        }
    }
}
#endif