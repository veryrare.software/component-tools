
using UnityEngine;

public static class BallisticProjectileHelper 
{

    public static void ShootRigidbody(Rigidbody projectileRb, float shootAngle, Vector3 target)
    {
        var velo = calcBallisticVelocityVector(projectileRb.transform.position, target,  45f);
        // Debug.Log("Computing ballistic vel from +:" + projectileRb.transform.position + " to "  + target + "   : " + velo);
        projectileRb.AddForce( velo, ForceMode.VelocityChange );
    }

    public static Vector3 calcBallisticVelocityVector(Vector3 initialPos, Vector3 finalPos, float angle)
    {
        var toPos = initialPos - finalPos;
        var h = toPos.y;
        toPos.y = 0;
        var r = toPos.magnitude;
        var g = -Physics.gravity.y;
        var a = Mathf.Deg2Rad * angle;
        var vI = Mathf.Sqrt (((Mathf.Pow (r, 2f) * g)) / (r * Mathf.Sin (2f * a) + 2f * h * Mathf.Pow (Mathf.Cos (a), 2f)));
        Vector3 velocity = (finalPos - initialPos).normalized * Mathf.Cos(a);
        velocity.y = Mathf.Sin(a);
        return velocity * vI;
    }

 
}
