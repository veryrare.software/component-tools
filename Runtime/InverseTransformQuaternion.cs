using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InverseTransformQuaternion
{
    public static Quaternion WorldToLocalRotation(Transform parent, Quaternion worldRotation)
    {
        return Quaternion.Inverse(parent.rotation) * worldRotation;
    }

    public static Quaternion LocalToWorldRotation(Transform parent, Quaternion localRotation)
    {
        return parent.rotation * localRotation;
    }


    public static Quaternion InverseTrasformRotation(this Transform parent, Quaternion worldRotation)
    {
        return WorldToLocalRotation(parent, worldRotation);
    }

    public static Quaternion TrasformRotation(this Transform parent, Quaternion localRotation)
    {
        return LocalToWorldRotation(parent, localRotation);
    }

}