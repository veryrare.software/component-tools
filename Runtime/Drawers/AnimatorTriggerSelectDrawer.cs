﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Reflection;
using System.Collections;


#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;

#endif

public class TriggerSelectorAttribute : PropertyAttribute
{
    public string animatorProperty = "Animator";

    public TriggerSelectorAttribute(string animatorProperty)
    {
        this.animatorProperty = animatorProperty;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(TriggerSelectorAttribute))]
public class TriggerSelectorPropertyDrawer : PropertyDrawer
{

    public static object GetPropValue(UnityEngine.Object src, string propName)
    {
        var prop = src.GetType().GetField(propName);
        return prop;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
 if (property.propertyType == SerializedPropertyType.String)
        {
            var attrib = this.attribute as TriggerSelectorAttribute;
            FieldInfo animProp = property.serializedObject.targetObject.GetType().GetField(attrib.animatorProperty);
            object animObj = animProp.GetValue(property.serializedObject.targetObject);
            Animator anim = animObj as Animator;

            EditorGUI.BeginProperty(position, label, property);

            if ( anim != null && anim.runtimeAnimatorController != null){

                var parameters = (anim.runtimeAnimatorController as AnimatorController).parameters;
                List<string> triggers = new List<string>(){ "<NoTrigger>" };
                int idx = 0;
                string val = property.stringValue;
                foreach( var p in parameters){
                    if ( p.type == AnimatorControllerParameterType.Trigger){
                        if ( p.name == val){
                            idx = triggers.Count;
                        }
                        triggers.Add(p.name);
                    }
                }
                
                idx = EditorGUI.Popup(position, property.name,  idx, triggers.ToArray());

                property.stringValue = triggers[idx];
            } else {
                property.stringValue = "<NoTrigger>";
            }

            EditorGUI.EndProperty();
        }
        else
        {
            EditorGUI.PropertyField(position, property, label);
        }
    }
}
#endif