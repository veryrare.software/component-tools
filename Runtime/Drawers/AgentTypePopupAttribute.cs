using System;
using UnityEngine;
using UnityEngine.AI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AgentTypePopup1Attribute : PropertyAttribute
{
}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(AgentTypePopup1Attribute))]
public class RangeDrawer : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // First get the attribute since it contains the range for the slider
        AgentTypePopup1Attribute range = attribute as AgentTypePopup1Attribute;

        if (property.propertyType == SerializedPropertyType.Integer)
        {
            var count = NavMesh.GetSettingsCount();
            var agentTypeNames = new string[count];
            var agentTypeIds = new int[count];
            for (var i = 0; i < count; i++)
            {
                var id = NavMesh.GetSettingsByIndex(i).agentTypeID;
                var name = NavMesh.GetSettingsNameFromID(id);
                agentTypeNames[i] = name;
                agentTypeIds[i] = id;
            }
            property.intValue = EditorGUI.IntPopup(position, property.intValue, agentTypeNames, agentTypeIds );
        }
    }
}
#endif
