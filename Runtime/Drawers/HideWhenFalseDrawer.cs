using UnityEngine;
using System.Collections;
 #if UNITY_EDITOR 
using UnityEditor;
#endif

public class HideWhenFalseAttribute : PropertyAttribute
{
    public readonly string hideBoolean;
 
    public HideWhenFalseAttribute (string booleanName)
    {
        this.hideBoolean = booleanName;
    }
}

#if UNITY_EDITOR 
[CustomPropertyDrawer(typeof(HideWhenFalseAttribute))]
public class HideWhenFalseDrawer : PropertyDrawer
{
    public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label)
    {
        var p = GetBoolPath(attribute as HideWhenFalseAttribute, property);
        var boolField = property.serializedObject.FindProperty(p);
        if(boolField == null || boolField.boolValue) EditorGUI.PropertyField(position, property, label, true);
    }
 
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var p = GetBoolPath(attribute as HideWhenFalseAttribute, property);
        var boolField = property.serializedObject.FindProperty(p);
        if(boolField != null && !boolField.boolValue) return 0f;
        return EditorGUI.GetPropertyHeight(property);
    }

    string GetBoolPath(HideWhenFalseAttribute hiddenAttribute, SerializedProperty property)
    {
        var idx = property.propertyPath.LastIndexOf('.');
        var boolPath = "";
        if ( idx > 0 ) boolPath = property.propertyPath.Substring(0, idx) + ".";
        boolPath += hiddenAttribute.hideBoolean;
        return boolPath;
    }

}
#endif