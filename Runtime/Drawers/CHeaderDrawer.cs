using UnityEngine;
using System.Collections;
using UnityEngine.UIElements;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class CHeaderAttribute : PropertyAttribute
{
    public string header;
    public float height = 40;

    public CHeaderAttribute(string header, float height = 40)
    {
        this.header = header;
        this.height = height;
    }
}

#if UNITY_EDITOR 
[CustomPropertyDrawer(typeof(CHeaderAttribute))]
public class CHeaderDrawer : PropertyDrawer
{

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float h = base.GetPropertyHeight(property, label);
        return h > 0.01f ? h+(attribute as CHeaderAttribute).height : 0f;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var h = base.GetPropertyHeight(property, label);
        if ( h > 0.01f){
             var boldtext = new GUIStyle (GUI.skin.label);
            boldtext.fontStyle = FontStyle.Bold;
            GUI.Label(position, (attribute as CHeaderAttribute).header, boldtext);
            position.y += (attribute as CHeaderAttribute).height;
        } 
        position.height -= (attribute as CHeaderAttribute).height;
        // EditorGUI.BeginProperty(position, label, property);
        EditorGUI.PropertyField(position, property, label);
        // EditorGUI.EndProperty();
    }

}
#endif