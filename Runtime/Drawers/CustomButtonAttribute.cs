using UnityEngine;
using System;
using System.Reflection;
#if UNITY_EDITOR
using UnityEditor;
#endif

[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = true)]
public class CustomButtonAttribute : PropertyAttribute
{
    public string [] methodNames;
    public bool replace;

    public CustomButtonAttribute(bool replace, params string [] methodNames)
    {
        this.methodNames = methodNames;        
        this.replace = replace;
    }
}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(CustomButtonAttribute))]
public class OpenFileDrawer : NestablePropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
		Initialize(property, true);
        var b = attribute as CustomButtonAttribute;
        var p1 = position;
        p1.width -= 100f * b.methodNames.Length;
        EditorGUI.PropertyField(p1, property, label);

        var p2 = position;
        p2.x += p1.width;
        p2.width = 100f;
        int i = 0;
        foreach(var m in b.methodNames)
        {
            p2.x += p2.width * (i++);
            if ( GUI.Button(p2, b.methodNames[0]) )
            {
                Type thisType = propertyObject.GetType();
                MethodInfo theMethod = thisType.GetMethod(b.methodNames[0]);
                Debug.Log("this type : " + thisType.ToString() + " " + theMethod);
                theMethod.Invoke(propertyObject, new object[]{});
            }
        }

    }

}

#endif
