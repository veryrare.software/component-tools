using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathHelpers
{
    static public float ModularClamp(float val, float min, float max, float rangemin = -180f, float rangemax = 180f) {
        var modulus = Mathf.Abs(rangemax - rangemin);
        if((val %= modulus) < 0f) val += modulus;
        return Mathf.Clamp(val + Mathf.Min(rangemin, rangemax), min, max);
    }

}
